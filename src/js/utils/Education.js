export class Education {
  constructor(edu) {
    this._year = edu.year;
    this._title = edu.title;
    this._description = edu.description;
  }

  get year() {
    return this._year;
  }

  get title() {
    return this._title;
  }

  get description() {
    return this._description;
  }
}
