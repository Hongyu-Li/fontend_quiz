import { Education } from './Education';

export class Person {
  constructor(data) {
    this._name = data.name;
    this._age = data.age;
    this._description = data.description;
    this._educations = data.educations.map(edu => new Education(edu));
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get description() {
    return this._description;
  }

  get educations() {
    return this._educations;
  }
}
