import $ from 'jquery';

export function renderAbout(person) {
  $('.about').html(person.description);
}
