import $ from 'jquery';

export function renderEducation(educations) {
  educations.forEach(edu => {
    $('ul').append(
      `<li><h4>${edu.year}</h4><div class = 'education'><h4>${
        edu.title
      }</h4><p>${edu.description}</p></div></li>`
    );
  });
}
