import { fetchData } from './utils/fetchData';
import { Person } from './utils/Person';
import { renderAbout } from './render/renderAbout';
import { renderEducation } from './render/renderEducation';
import { renderHeader } from './render/renderHead';

fetchData().then(data => {
  const person = new Person(data);
  const educations = person.educations;
  renderHeader(person);
  renderAbout(person);
  renderEducation(educations);
});
